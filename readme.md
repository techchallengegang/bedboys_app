# BedBoys App

## Ui Libraries

### Native Base

https://docs.nativebase.io/Components.html#Components
https://github.com/GeekyAnts/NativeBase#4-getting-started

npm install native-base
expo install expo-font

### React Native

https://reactnative.dev/docs/components-and-apis.html

### React Native Elements

https://react-native-elements.github.io/react-native-elements/docs/slider.html

npm install react-native-elements --save

### React Native Icons

List of icon imports: https://github.com/oblador/react-native-vector-icons/blob/master/Examples/IconExplorer/src/icon-sets.js
