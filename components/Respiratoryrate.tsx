import React from "react";
import { View, Text, StyleSheet, Vibration, Image } from "react-native";
import { Icon, colors } from "react-native-elements";
import { mainColor } from "../styles/colors";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import * as Animatable from "react-native-animatable";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { bbStyles } from "../styles/bbStyles";

type Props = {
  RESPvalue: string;
};

export const RespiratoryRate = (props: Props) => {
  return (
    <View
      style={{
        alignItems: "center",
        marginTop: 30,
        flex: 1,
        flexDirection: "row",
      }}
    >
      <Image
        style={{ width: 50, height: 50, marginTop: 10, opacity: 0.7 }}
        source={require("../assets/head_round_white.png")}
      />
      <Animatable.Text
        animation="flash"
        iterationCount="infinite"
        easing="linear"
        duration={10000}
      >
        <Image
          style={{ width: 20, height: 65 }}
          source={require("../assets/breath_white.png")}
        />
      </Animatable.Text>

      <Text
        style={[
          bbStyles.smallText,
          {
            color: "white",
            alignSelf: "center",
            marginLeft: 10,
            fontSize: 22,
            opacity: 0.8,
            marginRight: 30,
          },
        ]}
      >
        {props.RESPvalue}
      </Text>
    </View>
  );
};
