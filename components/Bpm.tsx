import React, { Component, useState } from "react";
import {
  AppRegistry,
  Animated,
  View,
  Text,
  StyleSheet,
  Vibration,
  Easing,
  Button,
} from "react-native";
import { colors } from "react-native-elements";
import { mainColor, orange } from "../styles/colors";

import Icon from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import { bbStyles } from "../styles/bbStyles";

type Props = {
  BPMvalue: string;
};

export const Bpm = (props: Props) => {
  return (
    <View>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          marginTop: 30,
        }}
      >
        <Animatable.Text
          animation="pulse"
          easing="ease-out"
          iterationCount="infinite"
          duration={1000}
        >
          <MaterialCommunityIcons
            name="heart-pulse"
            size={60}
            color={"white"}
            style={{ opacity: 0.8 }}
          />
        </Animatable.Text>

        <Text
          style={[
            bbStyles.smallText,
            {
              color: "white",
              alignSelf: "center",
              marginLeft: 10,
              fontSize: 22,
              opacity: 0.8,
              marginRight: 30,
            },
          ]}
        >
          {props.BPMvalue}
        </Text>
      </View>
    </View>
  );
};
