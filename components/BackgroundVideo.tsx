import { Video } from "expo-av";
import React, { useState, useEffect } from "react";
import { Animated, StyleSheet, Text, View } from "react-native";

type Props = {
  children: any;
  play: boolean;
};

export const VideoBackground = (props: Props) => {
  const [vid, setVid] = useState();
  const opacity = React.useMemo(() => new Animated.Value(0), []);
  const videoSource = require("../assets/Sunrise_animated.mp4");

  useEffect(() => {
    if (vid) {
      if (props.play) {
        vid.playAsync();
      } else {
        vid.stopAsync();
      }
    }
  }, [props.play]);

  //https://www.youtube.com/watch?v=9bQTBzMFfFk
  return (
    <View style={styles.container}>
      <View style={styles.background}>
        <Animated.View
          style={[styles.backgroundViewWrapper, { opacity: opacity }]}
        >
          <Video
            ref={(r) => setVid(r)}
            isLooping={false}
            isMuted
            positionMillis={0}
            onLoad={() => {
              console.log("Video loaded");
              Animated.timing(opacity, {
                toValue: 1,
                useNativeDriver: true,
              }).start();
            }}
            resizeMode="cover"
            shouldPlay={false}
            source={videoSource}
            style={{ flex: 1 }}
          />
        </Animated.View>
      </View>
      <View style={styles.overlay}>{props.children}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: "transparent",
    flex: 1,
    justifyContent: "center",
  },
  background: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "black",
  },
  backgroundViewWrapper: {
    ...StyleSheet.absoluteFillObject,
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "rgba(0,0,0,0.4)",
  },
  title: {
    color: "white",
    fontSize: 20,
    marginTop: 90,
    paddingHorizontal: 20,
    textAlign: "center",
  },
});
