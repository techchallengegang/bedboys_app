import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import LottieView from "lottie-react-native";
import { bbStyles } from "../styles/bbStyles";
import { orange } from "../styles/colors";

type Props = {
  play: boolean;
};

export const SleepingPhase = (props: Props) => {
  const [ani, setAni] = useState();
  const [speed, setSpeed] = useState(0);
  const [hour, setHour] = useState(6);
  const [min, setMin] = useState(59);

  useEffect(() => {
    console.log("Play Status: " + props.play);
    console.log("Speed " + speed);
    if (ani) {
      if (props.play) {
        console.log("PLAY");
        ani.play();
        setSpeed(1);
      } else {
        console.log("No PLAY");
        setSpeed(0);
      }
      return () => {};
    }
  }, [props.play]);

  return (
    <View style={styles.animationContainer}>
      <LottieView
        ref={(animation) => {
          setAni(animation);
        }}
        source={require("../assets/lottie/SleepPhases.json")}
        autoPlay
        style={{ height: 500 }}
        speed={speed}
      />

      <View
        style={{
          position: "absolute",
          height: 200,
          width: 200,
          marginEnd: 100,
          justifyContent: "flex-end",
          alignItems: "center",
          alignContent: "center",
        }}
      ></View>
    </View>
  );
};

const styles = StyleSheet.create({
  animationContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  buttonContainer: {
    paddingTop: 20,
  },
});
