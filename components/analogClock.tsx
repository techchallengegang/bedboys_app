import React from "react";

import Svg, { Circle, Rect, G, Line } from "react-native-svg";
import { View } from "react-native";

type Props = {
  degreeHour: number;
  degreeMin: number;
  size: string;
};

const Clock = (props: Props) => {
  return (
    <Svg height={props.size} width={props.size} viewBox="0 0 100 100">
      <Circle
        id="face"
        cx="50"
        cy="50"
        r="45"
        fill="#FFF"
        stroke="#000"
        opacity="0.5"
      />
      <G id="hands">
        <Rect
          id="hour"
          x="47.5"
          y="50"
          width="5"
          height="30"
          rx="2"
          ry="2"
          fill="#333"
          rotation={props.degreeHour}
          origin="50, 50"
        />
        <Rect
          id="min"
          x="48.5"
          y="50"
          width="3"
          height="40"
          rx="2"
          ry="2"
          fill="#333"
          rotation={props.degreeMin}
          origin="50, 50"
        />
        <Line
          id="sec"
          x1="5"
          y1="50"
          x2="10"
          y2="50"
          stroke="#333"
          stroke-width="1"
          fill="#FFF"
        />
        <Line
          id="sec"
          x1="95"
          y1="50"
          x2="90"
          y2="50"
          stroke="#333"
          fill="#FFF"
        />
        <Line
          id="sec"
          x1="50"
          y1="5"
          x2="50"
          y2="12"
          stroke="#333"
          fill="#FFF"
          strokeWidth="1.3"
        />
        <Line
          id="sec"
          x1="50"
          y1="95"
          x2="50"
          y2="90"
          stroke="#333"
          fill="#FFF"
        />
      </G>
    </Svg>
  );
};

export default Clock;
