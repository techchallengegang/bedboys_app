import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import LottieView from "lottie-react-native";
import { bbStyles } from "../styles/bbStyles";
import { orange } from "../styles/colors";

type Props = {
  play: boolean;
};

export const LottieClock = (props: Props) => {
  const [ani, setAni] = useState();
  const [speed, setSpeed] = useState(0);
  const [hour, setHour] = useState(6);
  const [min, setMin] = useState(59);

  useEffect(() => {
    let clockInterval: any;
    let keepClockTimeout: any;
    let innerClockTimeout: any;
    let clockTimeout: any;
    let leftHourInterval: any;
    let leftMinInterval: any;

    let operator = -1;
    let minIterations = 0;
    console.log("Play Status: " + props.play);
    console.log("Speed " + speed);
    if (ani) {
      if (props.play) {
        console.log("PLAY");
        ani.play();
        setSpeed(0);

        setHour(6);
        setMin(59);

        clockInterval = setInterval(() => {
          setSpeed((currentSpeed) => currentSpeed - 0.15 * operator);
        }, 750);
        leftHourInterval = setInterval(() => {
          setHour((hour) => (hour === 0 ? 0 : hour - 1));
        }, 4800);
        leftMinInterval = setInterval(() => {
          minIterations++;
          if (minIterations < 360) {
            setMin((currentMin) => (currentMin === 0 ? 59 : currentMin - 1));
          } else {
            setMin(0);
          }
        }, 80);
      } else {
        console.log("No PLAY");

        clearInterval(clockInterval);
        clearTimeout(clockTimeout);
        clearTimeout(innerClockTimeout);
        clearTimeout(keepClockTimeout);
        clearInterval(leftHourInterval);
        clearInterval(leftMinInterval);

        setSpeed(0);
        setHour(6);
        setMin(59);
      }
      clockTimeout = setTimeout(() => {
        operator = 0;
      }, 10000);
      keepClockTimeout = setTimeout(() => {
        clearTimeout(clockTimeout);
        operator = 1;
      }, 22000);
      innerClockTimeout = setTimeout(() => {
        console.log("ENDE");
        setSpeed(0);

        clearInterval(clockInterval);
        clearTimeout(keepClockTimeout);
        clearTimeout(innerClockTimeout);
        clearInterval(leftHourInterval);
        clearInterval(leftMinInterval);
        setHour(0);
        setMin(0);
      }, 33000);
      return () => {
        clearInterval(clockInterval);
        clearInterval(leftHourInterval);
        clearInterval(leftMinInterval);
      };
    }
  }, [props.play]);

  return (
    <View style={styles.animationContainer}>
      <View>
        <LottieView
          ref={(animation) => {
            setAni(animation);
          }}
          source={require("../assets/lottie/AnalogClockAllWhite.json")}
          autoPlay
          style={{
            opacity: 0.8,
            width: 100,
            height: 100,
            alignSelf: "flex-end",
          }}
          speed={speed}
        />
        <Text
          style={[
            bbStyles.smallText,
            {
              color: orange,
              alignSelf: "flex-end",
              marginRight: 16,
              fontSize: 22,
            },
          ]}
        >
          0{hour}:{min < 10 ? "0" + min : min}
        </Text>
      </View>

      <View
        style={{
          // position: "absolute",
          // height: 200,
          // // width: 200,
          // // marginEnd: 0,
          // justifyContent: "flex-end",
          // alignItems: "baseline",
          alignContent: "center",
        }}
      >
        {/* <Text
          style={[
            bbStyles.smallText,
            { color: "white", alignSelf: "center", fontWeight: "normal" },
          ]}
        >
          Remainig
        </Text>
        <Text
          style={[bbStyles.smallText, { color: "white", alignSelf: "center" }]}
        >
          Sleeping Time
        </Text> */}

        {/* <Text
          style={[
            bbStyles.smallText,
            { color: orange, alignSelf: "center", fontSize: 22 },
          ]}
        >
          0{hour}:{min < 10 ? "0" + min : min}
        </Text> */}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  animationContainer: {
    width: 500,
    height: 350,
    // justifyContent: "center",
    // alignItems: "center",
  },
  buttonContainer: {
    paddingTop: 20,
  },
});
