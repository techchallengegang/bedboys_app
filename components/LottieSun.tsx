import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import LottieView from "lottie-react-native";

export const LottieSun = () => {
  const [ani, setAni] = useState();

  useEffect(() => {
    if (ani) {
      ani.play(0, 17);
    }
  });

  return (
    <View style={styles.animationContainer}>
      <LottieView
        ref={(animation) => {
          setAni(animation);
        }}
        loop={false}
        source={require("../assets/lottie/IconSun.json")}
        style={{
          width: 400,
          height: 400,
        }}
        speed={0.2}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  animationContainer: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 50,
    paddingLeft: 10,
  },
});
