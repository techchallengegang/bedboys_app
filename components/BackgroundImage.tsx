import { Video } from "expo-av";
import React, { useState, useEffect } from "react";
import { Image, StyleSheet, Text, View, ImageBackground } from "react-native";

type Props = {
  from: boolean;
  children: any;
};

export const BackgroundImage = (props: Props) => {
  let imageSource;
  if (props.from) {
    imageSource = require("../assets/blue/background_blue_from.png");
  } else {
    imageSource = require("../assets/blue/background_blue.png");
  }

  return (
    <ImageBackground
      source={imageSource}
      style={{ width: null, height: null, flex: 1 }}
    >
      {props.children}
    </ImageBackground>
  );
};
