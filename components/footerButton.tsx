import {
  mainColor,
  darkWhite,
  buttonText,
  buttonColor,
} from "../styles/colors";
import { useNavigation, useLinkProps } from "@react-navigation/native";
import { View } from "react-native";
import { Button } from "react-native-elements";
import React from "react";

type Props = {
  text: string;
  route: string;
};

export const FooterButton = (props: Props) => {
  const navigation = useNavigation();

  return (
    <View
      style={{
        flex: 0,
        justifyContent: "flex-end",
        alignItems: "center",
        marginBottom: 25,
      }}
    >
      <Button
        title={props.text}
        titleStyle={{
          fontWeight: "bold",
          fontSize: 20,
          textAlign: "center",
          justifyContent: "center",
          color: buttonText,
        }}
        buttonStyle={{
          height: 50,
          backgroundColor: buttonColor,
          width: 200,
          borderRadius: 50,
          justifyContent: "center",
          marginBottom: 10,
        }}
        onPress={() => navigation.navigate(props.route)}
      />
    </View>
  );
};
