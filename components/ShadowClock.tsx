import React, { Component, useState, Children } from "react";
import {
  AppRegistry,
  Animated,
  View,
  Text,
  StyleSheet,
  Vibration,
  Easing,
  Button,
} from "react-native";
import Clock from "./AnalogClock";
import { useLinkProps } from "@react-navigation/native";
import { mainColor } from "../styles/colors";

type Props = {
  degreeHour: number;
  degreeMin: number;
  clockColor: string;
};

export const ShadowClock = (props: Props) => {
  return (
    <View style={[styles.circle, { shadowColor: props.clockColor }]}>
      <Clock
        degreeMin={props.degreeMin}
        degreeHour={props.degreeHour}
        size={"100%"}
      ></Clock>
    </View>
  );
};
const styles = StyleSheet.create({
  circle: {
    width: 380,
    height: 380,
    borderRadius: 400 / 2,
    backgroundColor: mainColor,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.7,
    shadowRadius: 150.0,
    elevation: 24,
  },
});
