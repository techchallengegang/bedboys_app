import { mainColor, darkWhite } from "../styles/colors";
import { useNavigation, useLinkProps } from "@react-navigation/native";
import { View, Platform } from "react-native";
import { Button } from "react-native-elements";
import React from "react";

type Props = {
  text: string;
  func: () => void;
};

export const FooterButtonPad = (props: Props) => {
  const navigation = useNavigation();

  return (
    <View
      style={{
        flex: 0,
        justifyContent: "flex-end",
        alignItems: "center",
        marginBottom: 25,
      }}
    >
      <Button
        title={props.text}
        titleStyle={{
          fontWeight: "bold",
          fontSize: Platform.isPad ? 20 : 20,
          textAlign: "center",
          justifyContent: "center",
          color: "white",
        }}
        buttonStyle={{
          height: Platform.isPad ? 65 : 50,
          backgroundColor: mainColor,
          width: Platform.isPad ? 300 : 200,
          borderRadius: Platform.isPad ? 30 : 50,
          justifyContent: "center",
          marginBottom: 10,
        }}
        onPress={props.func}
      />
    </View>
  );
};
