//export const mainColor = "#747f39";
//export const mainColor = "#2F70E3";

export const mainColor = "#f39200";
export const orange = "#f39200";
export const grey = "#304057";
export const lightGrey = "#acb3bd";
export const darkWhite = "#edf0f2";

//Color Code Logo
export const lightYellow = "#FAC05E";
export const midYellow = "#F5A623";
export const darkYellow = "#F39200";
export const lightBlue = "#B9CCE3";

export const buttonText = darkWhite;
export const buttonColor = mainColor;
export const text = lightBlue;
export const headTitle = mainColor;
export const disabledButton = "grey";

// export const mainColor = "#f39200";
// export const orange = "#f39200";
// export const grey = "#304057";
// export const lightGrey = "#acb3bd";
// export const darkWhite = "#edf0f2";
