import { mainColor } from "./colors";
import { View, Text, StyleSheet, Vibration } from "react-native";

export const bbStyles = StyleSheet.create({
  text: {
    textAlign: "center",
    fontSize: 30,
    color: "black",
  },
  smallText: {
    fontSize: 18,
    marginLeft: 5,
    alignSelf: "flex-start",
    fontWeight: "bold",
  },
  alarmButtonText: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 20,
  },
  title: {
    color: "white",
    fontWeight: "bold",
    textAlign: "left",
    fontSize: 65,
    backgroundColor: mainColor,
    paddingLeft: 10,
  },
  subtitle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "left",
    fontSize: 45,
    backgroundColor: mainColor,
    paddingLeft: 10,
  },
});
