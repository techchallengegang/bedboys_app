import * as React from "react";
import { Platform } from "react-native";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { HomeScreen } from "./screens/home";
import { RemoteScreen } from "./screens/remote";
import { AlarmScreen } from "./screens/alarm";
import { DemoScreen } from "./screens/demo";

const Stack = createStackNavigator();

import { SplashScreen } from "expo";
import { StartScreen } from "./screens/start";

function App() {
  SplashScreen.hide();
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Start"
        screenOptions={{
          headerShown: false,
          headerStyle: {
            backgroundColor: "white",
          },
          headerTintColor: "black",
        }}
      >
        {/* <Stack.Screen name="BedBoys" component={HomeScreen} /> */}
        <Stack.Screen
          name="Alarm"
          component={AlarmScreen}
          options={{ headerTitle: "" }}
        />
        <Stack.Screen name="Demo" component={DemoScreen} />
        <Stack.Screen name="Remote" component={RemoteScreen} />
        <Stack.Screen name="Start" component={StartScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
