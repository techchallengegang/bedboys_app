import { db } from "../config";

export const firebaseSetPower = (powerState: boolean) => {
  db.ref("bed").set({
    power: powerState,
  });
};

export const getRemoteState = () => {
  db.ref("state/").on("value", (snapshot) => {
    return snapshot;
  });
};

export const pressPower = (state: boolean) => {
  db.ref("remote").update({ power: state });
};

export const pressUp = (state: boolean) => {
  db.ref("remote").update({ up: state });
};

export const pressOk = (state: boolean) => {
  db.ref("remote").update({ ok: state });
};

export const pressDown = (state: boolean) => {
  db.ref("remote").update({ down: state });
};

export const speed = (state: number) => {
  db.ref("state").update({ speed: state });
};

export const amplitude = (state: number) => {
  db.ref("state").update({ amplitude: state });
};

export const duration = (state: number) => {
  db.ref("state").update({ duration: state });
};

export const alarmTime = (state: string) => {
  db.ref("alarm").update({ time: state });
};

export const alarmDay = (state: string) => {
  db.ref("alarm").update({ day: state });
};
