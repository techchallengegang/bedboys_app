import Firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyC0tT2QcZqc2Po2QzeCxJu0HbSkHCzdA38",
  authDomain: "tc-bedboys.firebaseapp.com",
  databaseURL: "https://tc-bedboys.firebaseio.com",
  projectId: "tc-bedboys",
  storageBucket: "tc-bedboys.appspot.com",
  messagingSenderId: "130545986359",
  appId: "1:130545986359:web:e91fc1f6777336391148b2",
  measurementId: "G-QQHE4NY898",
};

const app = Firebase.initializeApp(firebaseConfig);
export const db = app.database();
