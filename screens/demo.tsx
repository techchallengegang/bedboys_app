import * as React from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { View, Text, StyleSheet, Vibration } from "react-native";
import { bbStyles } from "../styles/bbStyles";
import { mainColor } from "../styles/colors";
import { db } from "../config";
import { useState, useEffect } from "react";
import { timeout } from "../utils/helper";
import { Button } from "react-native-elements";
import Clock from "../components/AnalogClock";
import { Bpm } from "../components/Bpm";
import { VideoBackground } from "../components/BackgroundVideo";
import { LottieClock } from "../components/LottieClock";
import LottieView from "lottie-react-native";
import { FooterButtonPad } from "../components/footerButtonPad";
import { RespiratoryRate } from "../components/Respiratoryrate";
import { pressPower } from "../logic/firebase";
import { SleepingPhase } from "../components/SleepingPhase";

export const DemoScreen = () => {
  const [inBedState, setInBedState] = useState(false);
  const [bpm, setBpm] = useState(51);
  const [rrate, setRrate] = useState(25);
  const [sleeping, setSleeping] = useState(false);
  const [clockIntervall, setClockIntervall] = useState(false);
  const [power, setPower] = useState();
  const [play, setPlay] = useState(false);
  const [wakingUp, setWakingUp] = useState(false);
  const [running, setRunning] = useState(false);

  useEffect(() => {
    if (sleeping) {
      console.log("Something is going on in sleeping");
      randomVital();
      setClockIntervall(true);
    }
    if (play) {
      db.ref("state/").off();
      db.ref("remote/").once("value", (snapshot) => {
        const power = snapshot.val().power;
        setPower(power);
        console.log("power state " + power);
      });
      setTimeout(() => {
        console.log("pressing Power");
        pressPower(!power);
        setWakingUp(true);
      }, 22000);
    }
  }, [inBedState, sleeping, play]);

  db.ref("sleep/").off();
  db.ref("sleep/").on("value", (snapshot) => {
    const inBed = snapshot.val().inBed;
    if (inBed !== inBedState) {
      setInBedState(inBed);
      if (!inBed) {
        setSleeping(false);
        setWakingUp(false);
      }
    }
  });

  const randomVital = async () => {
    if (sleeping) {
      const randomTime = (Math.random() * (10 - 3) + 3) * 1000;
      const bpm = Math.random() * (65 - 50) + 50;
      const rrate = Math.random() * (30 - 20) + 20;
      setBpm(~~bpm);
      await timeout(randomTime / 2);
      setRrate(~~rrate);
      await timeout(randomTime / 2);
      randomVital();
    }
  };

  return (
    <React.Fragment>
      <VideoBackground play={play}>
        <SafeAreaView
          style={{
            flex: 0,
            alignItems: "center",
            justifyContent: "center",
          }}
        />
        <Text
          style={[
            bbStyles.title,
            { marginTop: 0, backgroundColor: "transparent" },
          ]}
        >
          Demo
        </Text>
        <Text
          style={[
            bbStyles.subtitle,
            { marginTop: 0, backgroundColor: "transparent", marginBottom: 100 },
          ]}
        >
          Waking up with GoodMorning
        </Text>

        <View
          style={{
            flex: 0,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Text style={styles.mediumText}>Status:</Text>
          {!inBedState && !sleeping && !running && (
            <Text style={styles.text}>Waiting for user to go to bed.</Text>
          )}
          {inBedState && !sleeping && (
            <Text style={styles.text}>Waiting for user to fall asleep.</Text>
          )}
          {inBedState && sleeping && !wakingUp && (
            <Text style={styles.text}>User is sleeping.</Text>
          )}
          {inBedState && sleeping && wakingUp && (
            <Text style={styles.text}>Starting motor to wake user up.</Text>
          )}
          {!inBedState && running && (
            <Text style={styles.text}>Good Morning :)</Text>
          )}
          <View style={{ marginBottom: 50 }}></View>
          <View style={{}}>
            <LottieClock play={play}></LottieClock>
          </View>

          <View style={{ flexDirection: "row", marginTop: 15 }}>
            <View>
              <Bpm
                BPMvalue={
                  sleeping ? bpm.toString() : inBedState ? bpm.toString() : "-"
                }
              ></Bpm>
            </View>
            <View>
              <RespiratoryRate
                RESPvalue={
                  sleeping
                    ? rrate.toString()
                    : inBedState
                    ? rrate.toString()
                    : "-"
                }
              ></RespiratoryRate>
            </View>
          </View>
        </View>

        <View style={{ position: "absolute", left: 0, right: 0, bottom: 0 }}>
          <FooterButtonPad
            text={play ? "Reset" : "Start"}
            func={() => {
              if (inBedState) {
                setPlay(!play);
                setSleeping(true);
                setRunning(true);
              }
              if (!inBedState) {
                setPlay(false);
                setRunning(false);
              }
            }}
          ></FooterButtonPad>
        </View>
      </VideoBackground>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  text: {
    textAlign: "center",
    fontSize: 30,
    color: "white",
    //backgroundColor: "rgba(255,255,255, 0.5)",
  },
  smallText: {
    fontSize: 18,
    marginLeft: 5,
    fontWeight: "bold",
    color: "white",
  },
  mediumText: {
    fontSize: 22,
    marginLeft: 5,
    fontWeight: "bold",
    color: "white",
  },
  alarmButtonText: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 20,
  },
  alarmTime: {
    textAlign: "center",
    fontSize: 110,
    fontWeight: "bold",
    color: "black",
    marginTop: 20,
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
  },
});
