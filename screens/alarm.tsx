import * as React from "react";
import { View, Platform, StyleSheet, Switch } from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import { useState } from "react";
import { ButtonGroup, Text, Divider, Overlay } from "react-native-elements";
import { db } from "../config";
import { alarmTime, alarmDay } from "../logic/firebase";
import moment from "moment";
import { SafeAreaView } from "react-native-safe-area-context";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import {
  mainColor,
  grey,
  darkWhite,
  lightBlue,
  headTitle,
  text,
} from "../styles/colors";
import { BackgroundImage } from "../components/BackgroundImage";
import { FooterButton } from "../components/footerButton";

export const AlarmScreen = () => {
  const navigation = useNavigation();

  const [time, setTime] = useState(moment(new Date()).toISOString());
  const [show, setShow] = useState(false);
  const [index, setIndex] = useState([]);
  const [alarmState, setAlarmState] = useState(true);

  const setPickedTime = (event, selectedTime: Date | undefined) => {
    setShow(Platform.OS === "ios");
    alarmTime(moment(selectedTime).add(2, "hours").toISOString());
  };

  const showTimepicker = () => {
    setShow(!show);
  };

  const setAlarmTime = () => {
    setAlarmState(true);
    setShow(false);
    db.ref("alarm/").off();
  };

  const setCheckbox = async (index) => {
    //setIndex(index);
    alarmDay(JSON.stringify(index));
  };

  const buttons = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

  db.ref("alarm/").off();
  db.ref("alarm/").on("value", (snapshot) => {
    const firebaseTime = snapshot.val().time;
    const firebaseIndex = snapshot.val().day;

    if (firebaseTime !== time) {
      setTime(firebaseTime);
    }

    if (firebaseIndex !== JSON.stringify(index)) {
      console.log("3");
      setIndex(JSON.parse(firebaseIndex));
    }
  });

  return (
    <React.Fragment>
      <BackgroundImage from={false}>
        <SafeAreaView style={{ flex: 0 }} />
        <Text style={styles.title}>Good Morning</Text>

        <View style={styles.container}>
          <View
            style={{
              backgroundColor: "transparent",
              alignItems: "center",
              opacity: !alarmState ? 0.3 : 1,
            }}
          >
            <View style={{ marginTop: 80 }} />
            <Text style={styles.text}>Your Wake Up Time</Text>
            <TouchableOpacity onPress={showTimepicker}>
              <Text style={styles.alarmTime}>
                {moment(time).subtract(2, "hours").format("HH:mm")}
              </Text>
            </TouchableOpacity>
            <ButtonGroup
              // disabled={true}
              selectMultiple={true}
              selectedIndexes={index}
              buttons={buttons}
              containerStyle={{
                margin: 30,
                height: 30,
                backgroundColor: "transparent",
                borderColor: "transparent",
              }}
              selectedButtonStyle={{
                backgroundColor: mainColor,
                opacity: 0.5,
              }}
              disabledStyle={{ backgroundColor: "transparent" }}
              innerBorderStyle={{ color: "transparent" }}
              onPress={() => {}}
            />
          </View>
          <Text style={styles.alarmButtonText}>
            {alarmState ? "I will wake you up" : "I won't wake you up"}
          </Text>

          <Switch
            trackColor={{ false: darkWhite, true: mainColor }}
            thumbColor={alarmState ? darkWhite : darkWhite}
            ios_backgroundColor={grey}
            onValueChange={() => {
              setAlarmState(!alarmState);
            }}
            value={alarmState}
            style={{ marginTop: 20 }}
          ></Switch>

          <Overlay
            isVisible={show}
            onBackdropPress={setAlarmTime}
            overlayStyle={{
              position: "absolute",
              bottom: 0,
              width: "100%",
            }}
            animationType="fade"
          >
            <>
              <DateTimePicker
                testID="dateTimePicker"
                value={moment(time).subtract(2, "hours").toDate()}
                mode="time"
                is24Hour={true}
                display="default"
                onChange={(event, selectedTime) =>
                  setPickedTime(event, selectedTime)
                }
                style={{
                  shadowColor: "white",
                  shadowRadius: 0,
                  shadowOpacity: 1,
                  shadowOffset: { height: 0, width: 0 },
                }}
              />

              <Divider style={{ marginBottom: 5, height: 0 }}></Divider>
              <Text style={styles.smallText}>Active Days</Text>
              <ButtonGroup
                onPress={setCheckbox}
                selectMultiple={true}
                selectedIndexes={index}
                buttons={buttons}
                containerStyle={{
                  height: 30,
                  backgroundColor: "white",
                  borderColor: "white",
                  marginTop: 10,
                }}
                buttonStyle={{
                  borderColor: "white",
                }}
                underlayColor="red"
                selectedButtonStyle={{ backgroundColor: mainColor }}
                innerBorderStyle={{ color: "white" }}
              />
              <Divider style={{ marginBottom: 20, height: 0 }}></Divider>
            </>
          </Overlay>
        </View>
        <FooterButton text="Remote" route="Remote"></FooterButton>
      </BackgroundImage>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  text: {
    textAlign: "center",
    fontSize: 30,
    color: text,
  },
  smallText: {
    fontSize: 18,
    marginLeft: 5,
    alignSelf: "flex-start",
    fontWeight: "bold",
    color: text,
  },
  alarmButtonText: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 20,
    color: text,
  },
  title: {
    color: headTitle,
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 65,
    backgroundColor: "transparent",
    paddingLeft: 10,
  },
  alarmTime: {
    textAlign: "center",
    fontSize: 110,
    fontWeight: "bold",
    color: text,
    marginTop: 20,
  },
  container: {
    flex: 1,
    backgroundColor: "transparent",
    alignItems: "center",
  },
});
