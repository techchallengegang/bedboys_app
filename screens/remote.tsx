import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, Vibration } from "react-native";
import { Icon, colors } from "react-native-elements";
import Slider from "react-native-slider";
import { Button, Overlay } from "react-native-elements";
import { db } from "../config";
import { ListItem } from "react-native-elements";
import { speed, amplitude, duration, pressPower } from "../logic/firebase";

import { SafeAreaView } from "react-native-safe-area-context";
import { timeout } from "../utils/helper";
import { TouchableOpacity } from "react-native-gesture-handler";
import moment from "moment";
import {
  mainColor,
  grey,
  lightGrey,
  darkWhite,
  disabledButton,
  buttonColor,
  headTitle,
  text,
} from "../styles/colors";
import { BackgroundImage } from "../components/BackgroundImage";
import { FooterButton } from "../components/footerButton";

export const RemoteScreen = () => {
  const [powerState, setPowerState] = React.useState();
  const [speedState, setSpeedState] = React.useState(4);
  const [amplitudeState, setAmplitudeState] = React.useState(4);
  const [durationState, setDurationState] = React.useState(4);
  const [timer, setTimer] = React.useState("Sleep Timer");
  const [block, setBlockFunc] = React.useState(false);
  const [timerRunning, setTimerRunning] = useState(false);
  const [visible, setVisible] = useState(false);
  const [titleClickCount, setTitleClickCount] = useState(0);

  // Counts clicks on "Remote" and sets the values of "Amplitude", "Speed", and "Duration" to 0 if there are 5 clicks under 5secs
  const titlePress = () => {
    setTitleClickCount((prevCount) => prevCount + 1);
    checkTitleCount();
    setTimeout(() => {
      setTitleClickCount(0);
    }, 5000);
  };

  useEffect(() => {
    let init = true;
    if (init) {
      db.ref("state/").off();
      db.ref("state/").once("value", (snapshot) => {
        const speed = snapshot.val().speed;
        const amplitude = snapshot.val().amplitude;
        const duration = snapshot.val().duration;
        setSpeedState(speed);
        setAmplitudeState(amplitude);
        setDurationState(duration);
      });
    }
  });

  const checkTitleCount = async () => {
    if (titleClickCount === 4) {
      Vibration.vibrate();
      amplitude(1);
      speed(1);
      duration(1);
      setBlockFunc(true);
      await timeout(5000);
      setBlockFunc(false);
      setTitleClickCount(0);
    }
  };

  db.ref("remote/").off();
  db.ref("remote/").on("value", (snapshot) => {
    const power = snapshot.val().power;

    if (powerState !== power) {
      setPowerState(power);
    }
  });

  db.ref("state/").off();
  db.ref("state/").on("value", (snapshot) => {
    const speed = snapshot.val().speed;
    const amplitude = snapshot.val().amplitude;
    const duration = snapshot.val().duration;

    if (speedState !== speed && block) {
      setSpeedState(speed);
    }
    if (amplitudeState !== amplitude && block) {
      setAmplitudeState(amplitude);
    }
    if (durationState !== duration && block) {
      setDurationState(duration);
    }
  });

  const setTimerFunction = (value: number) => {
    setTimer(value);
  };

  const sendingValue = async (
    vibrate: boolean,
    value: number | boolean,
    firebaseFunc: (value: number | boolean) => {}
  ) => {
    if (vibrate) {
      Vibration.vibrate();
      if (typeof timer !== "number") {
        firebaseFunc(value);
      }
    } else {
      firebaseFunc(value);
    }
    setBlockFunc(true);
    await timeout(15000);
    setBlockFunc(false);
  };

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  useEffect(() => {
    if (typeof timer === "number" && timerRunning) {
      if (!timer) {
        pressPower(!powerState);
        setTimer("Timer");
        return;
      }
      const intervalId = setInterval(() => {
        setTimer(timer - 1);
      }, 1000);
      return () => clearInterval(intervalId);
    }
  }, [timer, timerRunning]);

  const list = [
    { title: "Off", value: "Timer", off: true },
    { title: "In 10 seconds", value: 10, off: false },
    { title: "In 15 minutes", value: 900, off: false },
    { title: "In 30 minutes", value: 1800, off: false },
    { title: "In 45 minutes", value: 2700, off: false },
    { title: "After I fell asleep", value: "Good Night", off: true },
  ];

  return (
    <React.Fragment>
      <BackgroundImage from={false}>
        <SafeAreaView style={{ flex: 0 }} />
        <TouchableOpacity
          onPress={titlePress}
          disabled={block}
          activeOpacity={1}
        >
          <Text style={styles.title}>Remote</Text>
        </TouchableOpacity>

        <View style={styles.container}>
          <View style={{ flex: 1, marginTop: 30 }}>
            <View style={{ flex: 1 }}>
              <Text style={styles.text}>{`Amplitude: ${
                amplitudeState * 12.5
              }% `}</Text>

              <Slider
                disabled={block}
                minimumValue={1}
                maximumValue={8}
                value={amplitudeState}
                step={1}
                onSlidingComplete={(value: number) =>
                  sendingValue(false, value, amplitude)
                }
                onValueChange={(value) => setAmplitudeState(value)}
                style={styles.slider}
                minimumTrackTintColor={block ? grey : mainColor}
                maximumTrackTintColor={lightGrey}
                thumbTintColor={block ? grey : mainColor}
              />
            </View>
            <View style={{ flex: 1 }}>
              <Text style={styles.text}>{`Speed: ${speedState * 12.5}%`}</Text>

              <Slider
                disabled={block}
                minimumValue={1}
                maximumValue={8}
                value={speedState}
                step={1}
                onSlidingComplete={(value: number) =>
                  sendingValue(false, value, speed)
                }
                onValueChange={(value: number) => setSpeedState(value)}
                style={styles.slider}
                minimumTrackTintColor={block ? grey : mainColor}
                maximumTrackTintColor={lightGrey}
                thumbTintColor={block ? grey : mainColor}
              />
            </View>
            <View style={{ flex: 1 }}>
              <Text
                style={styles.text}
              >{`Duration: ${durationState}0 min`}</Text>

              <Slider
                disabled={block}
                minimumValue={1}
                maximumValue={8}
                value={durationState}
                step={1}
                onSlidingComplete={(value: number) =>
                  sendingValue(false, value, duration)
                }
                onValueChange={(value: number) => setDurationState(value)}
                style={styles.slider}
                minimumTrackTintColor={block ? grey : mainColor}
                maximumTrackTintColor={lightGrey}
                thumbTintColor={block ? grey : mainColor}
              />
            </View>
            <Button
              style={styles.button}
              titleStyle={{ color: grey, marginLeft: 5 }}
              buttonStyle={{ borderColor: "transparent" }}
              onPress={() => {
                setTimerRunning(false);
                toggleOverlay();
              }}
              type="outline"
              icon={
                <Icon
                  name="brightness-3"
                  size={15}
                  color={grey}
                  style={{ transform: [{ rotate: "180deg" }] }}
                />
              }
              title={
                typeof timer == "number"
                  ? `${moment.utc(timer * 1000).format("mm:ss")} min`
                  : timer
              }
            />
          </View>
          <View>
            <Overlay
              overlayStyle={{
                position: "absolute",
                bottom: 0,
                width: "100%",
              }}
              backdropStyle={{ borderRadius: 20 }}
              isVisible={visible}
              onBackdropPress={toggleOverlay}
              ListItem={timer}
              animationType="fade"
            >
              <View style={{ width: 300, height: 300 }}>
                {list.map((item, i) => (
                  <ListItem
                    key={i}
                    title={item.title}
                    onPress={() => {
                      setTimerFunction(item.value);
                      toggleOverlay();
                    }}
                  />
                ))}
              </View>
            </Overlay>
          </View>

          <Button
            disabled={block}
            type="clear"
            style={styles.powerButtonOn}
            disabledStyle={styles.powerButtonOff}
            onPress={() => {
              typeof timer == "number" ? setTimerRunning(!timerRunning) : {};
              sendingValue(true, !powerState, pressPower);
            }}
            iconContainerStyle={{ borderBottomColor: "grey" }}
            icon={
              <Icon
                name="power-settings-new"
                size={55}
                color={darkWhite}
                backgroundColor="transparent"
              />
            }
          />
        </View>
        <FooterButton text="GoodMorning" route="Alarm"></FooterButton>
      </BackgroundImage>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  power: {
    textAlign: "center",
    fontSize: 20,
    flex: 1,
    color: buttonColor,
    marginTop: 80,
  },
  slider: {
    margin: 20,
    width: 250,
    height: 100,

    flex: 1,
    marginBottom: 70,
  },
  title: {
    color: headTitle,
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 65,
    //backgroundColor: mainColor,
    paddingLeft: 10,
  },
  text: {
    textAlign: "center",
    fontSize: 20,
    color: text,
  },

  container: {
    flex: 1,
    backgroundColor: "transparent",
    alignItems: "center",
  },

  button: {
    width: 300,
    backgroundColor: buttonColor,
    borderColor: "transparent",
    borderBottomColor: "transparent",
  },

  powerButtonOn: {
    borderRadius: 35,
    width: 70,
    height: 70,
    backgroundColor: buttonColor,
    alignItems: "center",
    marginTop: 30,
    marginBottom: 50,
  },
  powerButtonOff: {
    borderRadius: 35,
    width: 70,
    height: 70,
    backgroundColor: disabledButton,
    alignItems: "center",
  },
});
