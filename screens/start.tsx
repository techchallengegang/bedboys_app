import * as React from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { StyleSheet, Platform } from "react-native";
import { useEffect } from "react";
import { LottieSun } from "../components/LottieSun";
import { BackgroundImage } from "../components/BackgroundImage";
import { useNavigation } from "@react-navigation/native";

export const StartScreen = () => {
  const navigation = useNavigation();

  useEffect(() => {
    setTimeout(() => {
      navigation.navigate(Platform.isPad ? "Demo" : "Alarm");
    }, 5000);
  });
  return (
    <React.Fragment>
      <BackgroundImage from={true}>
        <SafeAreaView
          style={{
            flex: 0,
            alignItems: "center",
            justifyContent: "center",
          }}
        />
        <LottieSun></LottieSun>
      </BackgroundImage>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  text: {
    textAlign: "center",
    fontSize: 30,
    color: "white",
    //backgroundColor: "rgba(255,255,255, 0.5)",
  },
  smallText: {
    fontSize: 18,
    marginLeft: 5,
    fontWeight: "bold",
    color: "white",
  },
  mediumText: {
    fontSize: 22,
    marginLeft: 5,
    fontWeight: "bold",
    color: "white",
  },
  alarmButtonText: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 20,
  },
  alarmTime: {
    textAlign: "center",
    fontSize: 110,
    fontWeight: "bold",
    color: "black",
    marginTop: 20,
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
  },
});
