import * as React from "react";
import { Button, View, Text } from "react-native";
import { NavigationContainer, useNavigation } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

export const HomeScreen = () => {
  const navigation = useNavigation();
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Welcome to BedBoys</Text>
      <Button title="Open Alarm" onPress={() => navigation.navigate("Alarm")} />
      <Button title="Open Debug" onPress={() => navigation.navigate("Debug")} />
    </View>
  );
};
